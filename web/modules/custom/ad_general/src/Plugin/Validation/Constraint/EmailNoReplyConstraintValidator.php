<?php

namespace Drupal\ad_general\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class EmailNoReplyConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      if (strpos($item->value, 'no-reply') !== false) {
        $this->context->addViolation($constraint->message, ['%value' => $item->value]);
      }
    }
  }
}