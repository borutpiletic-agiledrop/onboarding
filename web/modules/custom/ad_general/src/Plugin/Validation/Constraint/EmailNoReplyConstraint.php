<?php

namespace Drupal\ad_general\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a unique integer.
 *
 * @Constraint(
 *   id = "EmailNoReply",
 *   label = @Translation("Unique Integer", context = "Validation"),
 *   type = "string"
 * )
 */
class EmailNoReplyConstraint extends Constraint {
  
  public $message = 'Please enter a valid email.';

}