<?php
namespace Drupal\ad_general\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ad_general\Service\DateStringService;

/**
 * List events.
 *
 * @Block(
 *   id = "ad_general_event_block",
 *   admin_label = @Translation("Event status")
 * )
 */
class EventBlock extends BlockBase {
        
    public function build() {
        
        $serviceDateString = \Drupal::service('ad_general.datestring');
        $eventStatusString = "";
        
        // Load date from event node
        $node = \Drupal::routeMatch()->getParameter('node');
        
        if ($node) {
          $node = \Drupal::entityTypeManager()->getStorage('node')->load($node->id());                
          $date = date("Y-m-d H:i:s", strtotime($node->field_event_date->getString()));

          $expiration = $serviceDateString->getDateExpiration($date);
          
          switch ($expiration['status']) {
              case DateStringService::DATE_STATUS_DONE:
                  $eventStatusString = $this->t("Event has already happened.");
              break;
              case DateStringService::DATE_STATUS_IN_PROGRESS:
                  $eventStatusString = $this->t("The event is in progress!");
              break;
              case DateStringService::DATE_STATUS_NOT_STARTED:
                  $eventStatusString = $this->t("There are @numberOfDays days left until this event starts.", ["@numberOfDays" => $expiration["daysLeft"]]);
              break;
          }
        }
        
        return [
            '#markup' => $eventStatusString,
            '#cache' => [
                'max-age' => 0
            ]
        ];
    }
}