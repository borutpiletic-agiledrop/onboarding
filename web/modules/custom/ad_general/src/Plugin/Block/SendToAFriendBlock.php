<?php
namespace Drupal\ad_general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ad_general\Form\SendToAFriendForm;
use \Drupal\Core\Entity\Plugin\DataType\EntityReference;

/**
 * Provides a 'SendToAFriendBlock' block.
 *
 * @Block(
 *  id = "SendToAFriendBlock",
 *  admin_label = @Translation("Send to a friend block"),
 * )
 */
class SendToAFriendBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function form() {
    $form = \Drupal::formBuilder()->getForm('Drupal\ad_general\Form\SendToAFriendForm');

    $eventName = $this->getLoadedEntityFieldValue('title');
    $eventLocation = $this->getReferencedEntityFieldValue('field_event_location', 'title');
    $eventDate = $this->getLoadedEntityFieldValue('field_event_date');
    $nid = $this->getLoadedEntityFieldValue('nid');
    $eventUrl = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$nid);
    
    $node = $this->getLoadedEntity();
    $eventUrl =  $node->toUrl()->toString();
    
    $form['eventName']['#value'] = $eventName;
    $form['eventLocation']['#value'] = $eventLocation;
    $form['eventDate']['#value'] = $eventDate;
    $form['eventUrl']['#value'] = $eventUrl;
  
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['form'] = $this->form();
    $build['#cache'] = [
      'max-age' => 0
    ];

    return $build;
  }

  public function getReferencedEntityFieldValue($entityReferenceField, $fieldName) {
    // Load date from event node
    $node = \Drupal::routeMatch()->getParameter('node');
    $fieldValue = '';

    if ($node) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($node->id()); 
      $entityReferenceEntity = $node->get($entityReferenceField)->first()->get('entity');
      
      if ($entityReferenceEntity instanceof EntityReference) {
        $fieldValue = $entityReferenceEntity->getTarget()->getValue()->get($fieldName)->get(0)->value;
      }
    }
    
    return $fieldValue;
  }
  
  public function getLoadedEntityFieldValue($fieldName) {
    // Load date from event node
    $node = \Drupal::routeMatch()->getParameter('node');
    $fieldValue = '';

    if ($node) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($node->id()); 
      $fieldValue = $node->get($fieldName)->get(0)->value;
    }
    
    return $fieldValue;
  }
  
  public function getLoadedEntity() {
    // Load date from event node
    $node = \Drupal::routeMatch()->getParameter('node');
    
    if ($node) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($node->id()); 
      return $node;
    }
    
    return null;
  }
}
