<?php
namespace Drupal\ad_general\Service;

class DateStringService {
    
    const SECONDS_IN_A_DAY = 86400;
    
    const DATE_STATUS_IN_PROGRESS = 'in_progress';
    
    const DATE_STATUS_NOT_STARTED = 'not_started';
    
    const DATE_STATUS_DONE = 'done';
    
    public $dateCompareFormat = 'Y-m-d';
    
    /**
     * Return a structure which indicates provided date expiration status. 
     * Method returns a following structure:
     * <li>daysLeft   : number of days before date starts
     * <li>status     : provided date status @see DateStringService::DATE_STATUS_* const.
     * 
     * @param array
     */
    public function getDateExpiration($dateStr) {
        $dateExpiration = [
            'daysLeft'  => 0,
            'status'    => ''
        ];
        $timezoneName = drupal_get_user_timezone();
        $timezone = new \DateTimeZone($timezoneName);
        
        $now = new \DateTime(); 
        $now->setTimezone(new \DateTimeZone('UTC'));
        $date = new \DateTime($dateStr, new \DateTimeZone('UTC'));
        $nextDay = new \DateTime(date("{$this->dateCompareFormat} 00:00:00", strtotime($dateStr) + self::SECONDS_IN_A_DAY));                
        $now->setTimezone($timezone);
        $date->setTimezone($timezone);
        
        if ($now > $date) {
            if ($date <= $nextDay && $now < $nextDay)
                $dateExpiration['status'] = self::DATE_STATUS_IN_PROGRESS;            
            else
                $dateExpiration['status'] = self::DATE_STATUS_DONE;
        }
        else if ($now->format($this->dateCompareFormat) == $date->format($this->dateCompareFormat)) {
            $dateExpiration['status'] = self::DATE_STATUS_IN_PROGRESS;
        }
        else {
            $daysLeft = ($date->getTimestamp() - $now->getTimestamp()) / self::SECONDS_IN_A_DAY;
            $daysLeft = ceil($daysLeft);            
            $dateExpiration['daysLeft'] = $daysLeft;
            $dateExpiration['status'] = self::DATE_STATUS_NOT_STARTED;
        }
        
        return $dateExpiration;
    }
}