<?php
namespace Drupal\ad_general\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;

class SendToAFriendForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'SendToAFriendForm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => '',
      '#required' => true
    ];
    $form['eventName'] = [
      '#type' => 'hidden'
    ];
    $form['eventLocation'] = [
      '#type' => 'hidden'
    ];
    $form['eventDate'] = [
      '#type' => 'hidden'
    ];
    $form['eventUrl'] = [
      '#type' => 'hidden'
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => '',
      '#required' => true
    ];
    $form['send'] = [
      '#type' => 'submit',
      '#title' => $this->t('Send'),
      '#default_value' => "Send"
    ];
    
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)){
      $form_state->setError($form['email'], 'Enter a valid email address');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    $mailConfig = [
      'module'    => 'ad_general',
      'key'       => 'SendToAFriend',
      'to'        => $form_state->getValue('email'),
      'langcode'  => \Drupal::currentUser()->getPreferredLangcode(),
      'send'      => true
    ];
    $params = [
      'name' => $form_state->getValue('name'),
      'eventName' => $form_state->getValue('eventName')
    ];

    $result = $mailManager->mail(
      $mailConfig['module'], 
      $mailConfig['key'], 
      $mailConfig['to'], 
      $mailConfig['langcode'], 
      $params, 
      NULL, 
      $mailConfig['send']
    );
    
    if ($result['result'] != true) {
      $message = t('There was a problem sending your email to @email.', array('@email' => $mailConfig['to']));
      drupal_set_message($message, 'error');
      \Drupal::logger('mail-log')->error($message);
      return;
    }

    $message = $this->t('An email has been sent to your friend @email about the "@eventName" event. ', array('@email' => $mailConfig['to'], '@eventName' => $params['eventName']));
    drupal_set_message($message);
    \Drupal::logger('mail-log')->notice($message);
  }

}